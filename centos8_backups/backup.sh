#!/bin/bash

source common.sh

# we want to maintain the same $SOURCE path to simplify restores, but we
# don't really want to read every snapshot on every run as this will take days
# Let's just backup $TODAY, ignoring symlink snaps (zero updates) as well
EXCLUDE_LIST=`mktemp`
RESTIC_LOGFILE=`mktemp`
find $SOURCE -mindepth 1 -maxdepth 1 \( -type l -o -type d \) ! -path "*$TODAY" > $EXCLUDE_LIST
$RESTIC backup --tag $TODAY --exclude-file=$EXCLUDE_LIST $SOURCE &>> $RESTIC_LOGFILE
rm -f $EXCLUDE_LIST

# Check if there are any snapshots to forget/purge
SNAPS_TO_REMOVE=`$RESTIC forget --dry-run --group-by paths --keep-within $PRUNE_SNAPSHOTS_OLDER_THAN | grep "remove .* snapshots" | awk '{print $2}'`
if [ ! -z $SNAPS_TO_REMOVE ]; then
  echo "Found $SNAPS_TO_REMOVE snapshots that are older than $PRUNE_SNAPSHOTS_OLDER_THAN. Purging from restic store ..." >> $RESTIC_LOGFILE
  $RESTIC unlock &>> $RESTIC_LOGFILE
  $RESTIC forget --group-by paths --keep-within $PRUNE_SNAPSHOTS_OLDER_THAN --prune &>> $RESTIC_LOGFILE
fi

echo "Sending email of $RESTIC_LOG to admins"
export TODAY="$TODAY"
export RESTIC_LOG="`cat $RESTIC_LOGFILE`"
rm -f $RESTIC_LOGFILE

envsubst < $TEMPLATE > email

cat email | ssmtp -t -v
rm -f email
