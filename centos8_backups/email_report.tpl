To: $EMAIL_ADMIN
From: $EMAIL_FROM
Reply-To: noreply.$EMAIL_FROM
Return-Path: $EMAIL_ADMIN
Subject: C8X - Backup for $TODAY

Dear Linux admins,

Today's backup ($TODAY) has completed with the following output:

$RESTIC_LOG

---
Best regards,
CERN Linux Droid
(on behalf of the friendly humans of Linux Support)
