job "${PREFIX}_centos8_backups" {
  datacenters = ["meyrin"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = false
  }

  reschedule {
    attempts       = 276
    interval       = "23h"
    unlimited      = false
    delay          = "5m"
    delay_function = "constant"
  }

  task "${PREFIX}_centos8_backups" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/centos8_backups/centos8_backups:${CI_COMMIT_REF_NAME}"
      force_pull = ${FORCE_PULL}
      logging {
        config {
          tag = "${PREFIX}_centos8_backups"
        }
      }
      volumes = [
        "$DATA:/data",
      ]
    }

    env {
      RESTIC_PASSWORD = "$RESTIC_PASSWORD"
      AWS_ACCESS_KEY_ID = "$AWS_ACCESS_KEY_ID"
      AWS_SECRET_ACCESS_KEY = "$AWS_SECRET_ACCESS_KEY"
      RESTIC_REPOSITORY = "s3:s3.cern.ch/$S3_REPOSITORY"
      PRUNE_SNAPSHOTS_OLDER_THAN = "$PRUNE_SNAPSHOTS_OLDER_THAN"
      PATH_SNAPSHOTS = "$PATH_SNAPSHOTS"
      EMAIL_FROM = "$EMAIL_FROM"
      EMAIL_ADMIN = "$EMAIL_ADMIN"
      NOMAD_ADDR = "$NOMAD_ADDR"
    }

    resources {
      cpu = 6000 # Mhz
      memory = 1024 # MB
    }

  }
}
